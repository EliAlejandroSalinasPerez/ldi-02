/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pkg10072014clase;

/**
 *
 * @author Eli
 */
public class Sumas {
    
    private  final static int TAM_BYTE=8;
    private  final static int TAM_PALABRA=16;
    private  final static int TAM_DOBLEPALABRA=32;
    private  final static int TAM_CUATRUPLEPALABRA=32;
  
    
     private  final static String DESBORDEBYTE="00000000";
     private  final static String DESBORDEPALABRA="0000000000000000";
     private  final static String DESBORDEDOBLEPALABRA="00000000000000000000000000000000";
     private  final static String DESBORDECUATRUPLEPALABRA="0000000000000000000000000000000000000000000000000000000000000000";
       
          
            
            
    public String SumaByte(String b1,String b2){
        
        if(b1.length()>TAM_BYTE || b2.length()>TAM_BYTE){
           return "Ingresa Bytes"; 
        }
        
       
        SumaBinaria byte1=new SumaBinaria();
        
        String total=byte1.Suma_Binaria(b1, b2);
        
        if(total.length()>TAM_BYTE){
            System.err.println(total+" NO ES UN BYTE");
            return DESBORDEBYTE;
            
        }
        else{
            return total;
        }
        
    }
     public String SumaBytePalabra(String byte0,String palabra){
        
        if(byte0.length()>TAM_BYTE || palabra.length()>TAM_PALABRA){
           return "Ingresa Bien los valores"; 
        }
        
       
        SumaBinaria byte1=new SumaBinaria();
        
        String total=byte1.Suma_Binaria(byte0, palabra);
        
        if(total.length()>TAM_PALABRA){
            System.err.println(total+" NO ES UNA PALABRA");
            return DESBORDEPALABRA;
            
        }
        else{
            return total;
        }
        
    }
        public String SumaByteDoblePalabra(String byte0,String doblePalabra){
        
        if(byte0.length()>TAM_BYTE || doblePalabra.length()>TAM_DOBLEPALABRA){
           return "Ingresa Bien los valores"; 
        }
        
       
        SumaBinaria byte1=new SumaBinaria();
        
        String total=byte1.Suma_Binaria(byte0, doblePalabra);
        
        if(total.length()>TAM_DOBLEPALABRA){
            System.err.println(total+" NO ES UNA DOBLEPALABRA");
            return DESBORDEDOBLEPALABRA;
            
        }
        else{
            return total;
        }
        
    }
          public String SumaByteCuatruplePalabra(String byte0,String cuatruplePalabra){
        
        if(byte0.length()>TAM_BYTE || cuatruplePalabra.length()>TAM_CUATRUPLEPALABRA){
           return "Ingresa Bien los valores"; 
        }
        
       
        SumaBinaria byte1=new SumaBinaria();
        
        String total=byte1.Suma_Binaria(byte0, cuatruplePalabra);
        
        if(total.length()>TAM_CUATRUPLEPALABRA){
            System.err.println(total+" NO ES UNA CUATRUPLEPALABRA");
            return DESBORDECUATRUPLEPALABRA;
            
        }
        else{
            return total;
        }
        
    }
   
    public String SumaPalabra(String palabra,String palabra2){
        
        if(palabra.length()>TAM_PALABRA || palabra2.length()>TAM_PALABRA){
            return "Ingrese PALABRA";
        }
        
        
        SumaBinaria byte1=new SumaBinaria();
        
        String total=byte1.Suma_Binaria(palabra, palabra2);
        
        if(total.length()>TAM_PALABRA){
            System.err.println(total+" No es una palabra");
            return DESBORDEPALABRA;
            
        }
        else{
            return total;
        }

    }
        public String SumaPalabraDoblePalabra(String palabra,String doblepalabra){
        
        if(palabra.length()>TAM_PALABRA || doblepalabra.length()>TAM_DOBLEPALABRA){
            return "Ingrese PALABRA";
        }
        
        
        SumaBinaria byte1=new SumaBinaria();
        
        String total=byte1.Suma_Binaria(palabra, doblepalabra);
        
        if(total.length()>TAM_DOBLEPALABRA){
            System.err.println(total+" No es una doble palabra");
            return DESBORDEDOBLEPALABRA;
            
        }
        else{
            return total;
        }

    }
     public String SumaPalabraCuatruplePalabra(String palabra,String cuatruplepalabra){
        
        if(palabra.length()>TAM_PALABRA || cuatruplepalabra.length()>TAM_CUATRUPLEPALABRA){
            return "Ingrese PALABRA";
        }
        
        
        SumaBinaria byte1=new SumaBinaria();
        
        String total=byte1.Suma_Binaria(palabra, cuatruplepalabra);
        
        if(total.length()>TAM_CUATRUPLEPALABRA){
            System.err.println(total+" No es una Cuatruple palabra");
            return DESBORDECUATRUPLEPALABRA;
            
        }
        else{
            return total;
        }

    }
     
     public String SumaDoblePalabra(String doblepalabra,String doblepalabra2){
         if (doblepalabra.length()>TAM_DOBLEPALABRA || doblepalabra2.length()>TAM_DOBLEPALABRA) {
             return "Ingresa Doubleword";
             
         }
        
        SumaBinaria byte1=new SumaBinaria();
        
        String total=byte1.Suma_Binaria(doblepalabra, doblepalabra2);
        
        if(total.length()>TAM_DOBLEPALABRA){
            System.err.println(total+" NO ES UNA DOBLE PALABRA");
            return DESBORDEDOBLEPALABRA;
            
        }
        else{
            return total;
        }

    }
     
        public String SumaDoblePalabraCuatruplePalabra(String doblepalabra,String cuatruplepalabra){
         if (doblepalabra.length()>TAM_DOBLEPALABRA || cuatruplepalabra.length()>TAM_CUATRUPLEPALABRA) {
             return "Ingresa valores correctos";
             
         }
        
        SumaBinaria byte1=new SumaBinaria();
        
        String total=byte1.Suma_Binaria(doblepalabra, cuatruplepalabra);
        
        if(total.length()>TAM_CUATRUPLEPALABRA){
            System.err.println(total+" NO ES UNA cautruple PALABRA");
            return DESBORDECUATRUPLEPALABRA;
            
        }
        else{
            return total;
        }

    } 

     
         public String SumaCuatruplePalabra(String cuatruplepalabra,String cuatruplepalabra2){
             if (cuatruplepalabra.length()>TAM_CUATRUPLEPALABRA || cuatruplepalabra2.length()>TAM_CUATRUPLEPALABRA) {
                 return "Ingresa Cuatruple palabra";
             }
             
        SumaBinaria byte1=new SumaBinaria();
        
        String total=byte1.Suma_Binaria(cuatruplepalabra, cuatruplepalabra2);
        
        if(total.length()>TAM_CUATRUPLEPALABRA){
            System.err.println(total+" NO ES UNA CUATRUPLE PALABRA");
            return DESBORDECUATRUPLEPALABRA;
            
        }
        else{
            return total;
        }

    }
     
         /*
    public static void main(String[] args) {
        Sumas sum=new Sumas();
        System.out.println("Suma Bytes "+
        sum.SumaByte("11111111", "1"));
        
        
        System.out.println("Suma Palabras "+
        sum.SumaPalabra("11111111111111", "1111"));
        
        System.out.println("Suma Dobles "+
        sum.SumaDoblePalabra("111111111111111111111111111111111111111111", "1111"));
        
        System.out.println("Suma Cuatruple "+
        sum.SumaCuatruplePalabra("111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111", "1111"));
    }*/
    
    
    
    
}
